'use strict'

const { hooks } = require('@adonisjs/ignitor')

hooks.before.httpServer(() => {
  const View = use('View')
  const Config = use('Config')
  const Env = use('Env')

  if (Env.get('NODE_ENV') !== 'development') {
    const manifest = Config.get('webpack_manifest')
    View.global('prod', 'true')
    View.global('runtime', manifest.runtime.js)
    View.global('vendors', manifest.vendors.js)
    View.global('app_css', manifest.app.css)
    View.global('app_js', manifest.app.js)
  }
})
