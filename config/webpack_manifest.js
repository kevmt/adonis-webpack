module.exports = {
  "app": {"css": "/app.f8961df0.css", "js": "/app.f8961df0.js"},
  "vendors": {"js": "/vendors.f527358c.js"},
  "runtime": {"js": "/runtime.5c50c64a.js"},
  "": {"svg": ["/images/logo.svg", "/images/title.svg"], "png": ["/images/splash.png", "/images/pyramid.png"]}
}
