const webpack = require('webpack')
const dirs    = require('./watchDirectories')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')

/**
 * Development
 */
let config = {
  mode: 'development',
  devtool: 'inline-source-map',
  output: {
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              outputStyle: 'expanded'
            }
          }
        ]
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new FriendlyErrorsPlugin()
  ]
}

module.exports = new Promise((resolve, reject) => {
  const paths = dirs()
  paths
    .then(folders => {
      console.log('folders', folders)
      config.devServer = {
        open: true,
        openPage: process.env.WEBPACK_URL || '',
        host: process.env.WEBPACK_HOST,
        port: process.env.WEBPACK_PORT,
        proxy: {
          '**': `http://${process.env.HOST}:${process.env.PORT}`
        },
        hot: true,
        noInfo: true,
        overlay: true,
        clientLogLevel: 'none',
        watchContentBase: true,
        contentBase: folders,
        watchOptions: {
          poll: true
        }
      }
      resolve(config)
    })
    .catch(err => reject(err))
})
