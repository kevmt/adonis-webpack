const path = require('path')
const glob = require('glob')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const ImageminPlugin = require('imagemin-webpack-plugin').default
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const AssetsPlugin = require('assets-webpack-plugin')

/**
 * Production
 */
const config = {
  mode: 'production',
  output: {
    filename: '[name].[chunkhash:8].js'
  },
  optimization: {
    minimizer: [
      new UglifyJSPlugin({
        parallel: true,
        cache: true,
        sourceMap: true,
        uglifyOptions: {
          output: {
            comments: false
          },
          compress: {
            drop_console: true
          }
        }
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          safe: true,
          discardComments: { removeAll: true }
        }
      })
    ],
    concatenateModules: true,
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    },
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: false,
              sourceMap: false,
              importLoader: 2,
              minimize: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: () => [
                require('postcss-preset-env')()
              ]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: false
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['public'], {
      root: path.resolve(''),
      exclude: ['browserconfig.xml', 'site.webmanifest'],
      verbose: true,
      dry: false
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[chunkhash:8].css'
    }),
    new ImageminPlugin({
      externalImages: {
        context: 'resources',
        sources: glob.sync('resources/img/*'),
        destination: 'public'
      }
    }),
    new ImageminPlugin({
      externalImages: {
        context: 'resources',
        sources: glob.sync('resources/svgs/*'),
        destination: 'public'
      }
    }),
    new AssetsPlugin({
      filename: 'webpack_manifest.js',
      path: path.join('', 'config'),
      processOutput: function (assets) {
        return 'module.exports = ' + JSON.stringify(assets)
      }
    })
  ]
}

module.exports = new Promise(resolve => resolve(config))
