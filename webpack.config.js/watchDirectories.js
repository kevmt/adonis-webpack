const path = require('path')
const readdirp = require('readdirp')

/**
 * Watch Directories is a utility function for building an array of file
 * paths that Webpack uses to monitor for changes. The Webpack file watcher
 * doesn't support globs like `**` so we have to construct a path for every
 * folder in the directories we want to watch for changes
 * @returns {Promise<any>}
 */
module.exports = function watchDirectories () {
  let filePaths = []
  return new Promise((resolve, reject) => {
    const resources = path.resolve('resources/views') // 'resources' only will trigger full page refresh whenever you save js or sass
    const assets = path.resolve('public')

    filePaths.push(resources)
    filePaths.push(assets)

    // Read through the directories and find all subfolders.
    // Add them to the filepaths array that we resolve with.
    readdirp({
      root: path.resolve(''),
      directoryFilter: function (di) {
        const dirPath = di.path
        // only return directories in the resources or public directory
        return (dirPath.indexOf('resources') !== -1 || dirPath.indexOf('public') !== -1)
      },
      entryType: 'directories'
    }).on('data', entry => {
        // only watch the views/public directories because Webpack takes care of the rest
        // without this, HMR won't work properly and the page will always hard refresh
        if (entry.parentDir.indexOf('public') !== -1 || entry.parentDir.indexOf('views') !== -1) {
          filePaths.push(entry.fullPath)
        }
      })
      .on('error', err => reject(err))
      .on('end', () => resolve(filePaths))
  })
}
