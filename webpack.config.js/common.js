const webpack = require('webpack')
const path = require('path')

/**
 * Common maintains our base config that remains the same whether in
 * development or production.
 */
module.exports = {
  entry: {
    app: './resources/js/app.js'
  },
  output: {
    publicPath: '/',
    path: path.resolve('public')
  },
  resolve: {
    extensions: ['.js'],
  },
  module: {
    rules: [

      // Enforce our linting standards before anytning else
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loaders: 'eslint-loader',
        enforce: 'pre',
        options: {
          formatter: require('eslint-friendly-formatter'),
          quiet: true
        }
      },

      // JavaScript
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true
          }
        }
      },

      // Webfonts
      {
        test: /\.(eot|ttf|woff2?)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[hash:8].[ext]',
              outputPath: 'webfonts'
            }
          }
        ]
      },

      // Expose jQuery to the global scope
      {
        test: require.resolve('jquery'),
        use: [
          {
            loader: 'expose-loader',
            options: 'jQuery'
          },
          {
            loader: 'expose-loader',
            options: '$'
          }
        ]
      },

      // Images
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        enforce: 'pre',
        use: [
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true
            }
          },
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    // Ensure all jQuery references use the same library/version
    new webpack.ProvidePlugin({
      $: 'jquery',
      jquery: 'jquery',
      'window.jQuery': 'jquery',
      jQuery: 'jquery'
    }),
  ]
}
